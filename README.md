
# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [A Tecnologia na Educação](capitulos/tecnologia_educacao.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Redes neurais](capitulos/redes_neurais.md)



## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://secure.gravatar.com/avatar/72a8e3b16ebbdbcd377675c9ce22cc43?s=200&d=identicon)  | Renan Bela Cescon | RenanCescon | [renancescon@alunos.utfpr.edu.br](mailto:renancescon@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11359593/avatar.png?width=400) | Felipe de souza | Felipesouza003 | [felipesouza.2003@alunos.utfpr.edu.br](mailto:felipesouza.2003@alunos.utfpr.edu.br)
| ![](https://secure.gravatar.com/avatar/74b5bd7b6dabd33767e9590912fe02e3?s=200&d=identicon)  | Vitor Galli | 25Galli | [galli@alunos.utfpr.edu.br](mailto:galli@alunos.utfpr.edu.br)
