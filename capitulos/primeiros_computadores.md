# Das primeiras máqunas de calcular até os computadores quânticos.
Hoje os computadores são ferramentas indispensáveis para o desenvolvimento do mundo moderno e da ciência. Embora sejam de uso recente,sua história remonta a tempos antigos que vão desde as ferramentas de cálculo, passando pela revolução industrial e suas tentativas de se criar computadores mecânicos, até chegar a forma dos computadores eletrônicos
conhecida hoje[1].
##  As primeiras calculadoras

![](../imagens/pascal.jpg)

A primeira calculadora mecânica registrada na história foi desenvolvida pelo francês
Blaise Pascal (1623-1662) e batizada de La Pascaline, em 1642. E segundo Fonseca (2007)
foi montada para ajudar nos negócios do pai.
Essa calculadora realizava operações de soma e subtração e tinha a capacidade de
memorização e armazenamento dos resultados, as operações de multiplicação e divisão
podiam ser feitas por repetição. Em 1671, essa mesma calculadora foi aperfeiçoada por
Gottfried Leibniz (1646-1716)[2].

## Os primeiros computadores


![](../imagens/engenhoanalitico.jpg)


O matemático Charles Babbage (1792-1871) e sua assistente Ada Augusta Byron
King, (1815-1852) contribuíram com a projeção do que hoje se considera como os primeiros
computadores do mundo: a Máquina de Diferenças (em 1822 com a função de tabular funções
polinomiais) e a Máquina Analítica (em 1833 para desenvolver diversos tipos de cálculo)[2].
 
 Essa última, como é citado por Fonseca (2007) e estava muito próxima conceitualmente
daquilo que hoje é chamado de computador.
Ada Byron é considerada como a primeira programadora de computadores da história
por sua capacidade de imaginar e descrever estruturas como desvio
condicional, o laço condicional e as sub-rotinas, conceitos que foram
incorporados aos computadores e que são essenciais para seu
funcionamento. (WAZLAWICK, 2016)[2].

## O surgimento da programação
É no século XIX que um dos principais conceitos da computação moderna começou a ser desenvolvido: a programacão. Antes disso, por mais que tivéssemos máquinas com engrenagens que permitissem somar, subtrair,multiplicar e dividir, não tínhamos algo que poderia ser programado.[1]
### Babbage 

![](../imagens/Jacquard.jpeg)

Charles Babbage foi um homem muito a frente de seu tempo. Seu pai
foi um banqueiro abastado da Inglaterra. Graças a sua condição financeira Babbage teve ótimos tutores durante sua infância o que possibilitou que ingressasse em Cambridge. Durante seus estudos Babbage participou de diversas fraternidades em Cambridge, foi neste momento que desenvolveu o interesse pela hipótese de um computador programável.

A máquina diferencial de Babbage consistia em uma máquina capaz de
calcular polinômios, mas seu design completo não foi concluído na época embora exista hoje uma reproducão funcional desta mesma máquina no
Museu de Ciências de Londres.[1]
![](../imagens/babage.jpg) 

## A era atual
Apesar dessas dificuldades, deve-se continuar a chamar a atenção sobre a
importância de se registrar e estudar o desenvolvimento dos computadores eletrônicos e a
conseqüente evolução dos temas anexos: Linguagens de Programação, a Teoria da
Computação, estudo da Complexidade dos Algoritmos, etc., assim como a importância
decisiva do fator humano. Quando tantos se maravilharam com a derrota do campeão
mundial de xadrez Kasparov para o computador da IBM, o Deep Blue, (abril/maio de 1997),
surpreende a pouca atenção dada à equipe de técnicos que construiu e programou a máquina, 
às heurísticas utilizadas e aos objetivos que estão por detrás desse novo engenho, como se
alguém ficasse maravilhado com o quadro e os pincéis de uma obra de arte e se esquecesse do
artista. A história tem o dom de focalizar com especial nitidez aquele que é o seu personagem
principal: o homem. (FONSECA FILHO, 2007)

### Computadores quânticos 

![](../imagens/quantico.jpg)

O computador quântico é um dispositivo que utiliza os fenômenos quânticos para armazenar e processar informação. Enquanto num computador clássico os registos de memória podem assumir apenas um de dois estados, num computador quântico cada registo está num estado aparentemente estranho que contém simultaneamente propriedades de ambos estados clássicos. Também a forma como a informação é processada num computador quântico é completamente diferente e exige um controle muito fino da matéria e das interações que ocorrem entre os seus constituintes. Se é certo que as capacidades dos primeiros protótipos sejam ainda muito limitadas, espera-se que estas venham rapidamente a ultrapassar as dos computadores atuais, quer em velocidade quer por permitirem realizar tarefas até agora inacessíveis[3].

# Referências
1. CREPALDI, Caike; COSTA, Leonardo V.; ESCOBAL, Anderson A. A História da Computação: Das Máquinas de Calcular aos Computadores Quânticos. Instituto de Física da Universidade de São Paulo, IF-USP, v. 9, 2017.
2. SILVA, Sidnéia Almeida et al. MATEMÁTICOS NA HISTÓRIA DA COMPUTAÇÃO.
3. GUERREIRO, Ariel. Computadores Quânticos. Revista de Ciência Elementar, v. 6, n. 3, 2018.
4. FONSECA FILHO, Cléuzio. História da computação: O Caminho do Pensamento e da Tecnologia. EDIPUCRS, 2007.
