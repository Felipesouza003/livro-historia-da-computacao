# A tecnologia na educação

![](../imagens/imgTecnologia2.jpg)
 
A tecnologia é por definição, segundo o dicionário de Oxford Languages, uma teoria geral e/ou estudo sistemático sobre técnicas, processos, métodos, meios e instrumentos de um ou mais ofícios ou domínios da atividade humana. Essa definição por si só mostra quão amplo é este conceito e por isso ele acaba sendo alvo de erros de compreensão por grande parte das pessoas. Isso afeta diretamente sua característica mais forte: a ampla gama de situações em que pode ser útil. Um exemplo disso, e o que espero discutir neste capítulo, é a utilização da tecnologia na educação. 

Grande parte dos educadores têm receio quanto ao uso da tecnologia nas escolas e acabam incitando discursos contra o seu uso. Porém, a tecnologia sempre esteve presente nas escolas e universidades pois, segundo Eduardo O C Chaves, a fala humana, a escrita, e, consequentemente, aulas, livros e revistas, para não mencionar currículos e programas, são tecnologia, e que, portanto, educadores vêm usando tecnologia na educação há muito tempo. É apenas a sua familiaridade com essas tecnologias que as torna transparentes (i.e., invisíveis) a eles.[1]
Todavia, atualmente o conceito de tecnologia está mais “focado” no âmbito computacional e portanto devemos focar nossa análise na utilização da computação no meio estudantil. 

![](../imagens/aluno.jpg)

Segundo um estudo sobre a qualidade da educação de 70 países realizado pela OCDE (Organização para Cooperação e Desenvolvimento Econômico) e pelo PISA (Programa Internacional de Avaliação de Estudantes) em 2015, o Brasil ficou em na 66º posição em Matemática, apresentando 70% de estudantes abaixo do nível básico, o que mostra a necessidade de uma melhoria na qualidade de ensino do país.

# Robo Euroi

Melhoria essa que pode ser proporcionada pela utilização da tecnologia em escolas e universidades. Um exemplo disso seria a proposta de utilização de tecnologias como o Robo Euroi, jogo de estratégia matemática para exercitar o pensamento computacional[2]. Com o intuito de melhorar a qualidade do ensino em matemática os autores, Douglas Silva de Melo, Fernanda Gabriela de Sousa Pires, Rafaela Melo Ferreira, Robson James dos Reis Silva Junior, escreveram um artigo sobre essa nova tecnologia conhecida como o Robo Euroi e nesse artigo eles explicam por que seria interessante o uso dessa tecnologia e como ela funciona. Esse é um trecho retirado da introdução do artigo citado e mostra como funciona o aprendizado de matemática e como ele pode ser amplificado com o uso dessa nova tecnologia: 
Tendo em vista que a Matemática é uma atividade lógica, é preciso analisar e inserir um método de aprendizagem que vise reforçar o pensamento lógico e analítico para uma possível melhora nos índices apresentados. Uma alternativa para alinhar a Matemática e o pensamento lógico, levando em consideração a resolução de problemas, é a utilização de tecnologias e a inserção do Pensamento Computacional (PC) no currículo escolar. Para Wing (2006, 2008), Pensamento Computacional é um conjunto de habilidades necessárias para qualquer pessoa, e não somente profissionais de computação, essas mesmas habilidades são importantes para a resolução de problemas. Apesar deste conceito estar inserido na BNCC – Base Nacional Comum Curricular – (MEC, 2017), é pouco visto nas escolas, e pouco reforçado. Uma das formas de reforçar o PC, é a utilização de jogos digitais. Segundo Gros (2007), a aprendizagem é geralmente ligada a algo monótono, mas quando a pessoa se sente ligada a uma atividade que proporciona satisfação, ela consegue dedicar tempo e esforço para a tarefa. Os jogos educacionais envolvem muitas dessas características, tornando-se uma alternativa para que as aulas sejam mais interativas. Tarouco (2004) aponta que os jogos podem ser objetos de aprendizagem bastante impactantes, já que despertam o interesse e competição saudável e autonomia do jogador, além de exercitar os conhecimentos já existentes. Os jogos digitais possuem ambientes atraentes que, de certa forma, interagem com o jogador e capturam a atenção dele ao oferecer desafios que exigem níveis crescentes de habilidades (Leite & Godoy, 2013). O presente artigo apresenta a proposta do jogo “Robô Euroi”, voltado para a disciplina de Matemática, com foco na associação de número e quantidade, para crianças na fase de alfabetização[2].

Este é um dos exemplos das tecnologias atuais e como elas podem facilitar o ensino nas escolas, mas nós percorremos um caminho com altos e baixos para chegar na situação atual e para auxiliar na compreensão de como foi a história dessa introdução da tecnologia à educação o autor Demerval Guilarducci Bruzzi construiu um artigo sobre esse percurso. Aqui está o resumo do mesmo: 

# História da tecnologia na educação
![](../imagens/historia1.jpg)

Fruto de minha pesquisa de doutorado, o artigo tem como principal intenção apresentar um percurso histórico contemplando a evolução da tecnologia disponível a educação desde os idos de 1850 até os dias atuais, apontando de forma clara e direta ao leitor o caminho evolutivo das tecnologias, bem como suas possíveis aplicações, deixando claro que a educação desde os primórdios até os dias atuais sempre teve contato com algum tipo de tecnologia, contrariando alguns teóricos que atribuem a salvação da educação na adoção de um processo tecnológico[3].  
Introdução:     
No Brasil falamos muito sobre o uso da tecnologia na educação, mas pouco ainda fazemos. É claro que temos diversos projetos de sucesso, mas nenhum até hoje apresentou capacidade de capilaridade e profundidade para sua difusão. Devido a minha pesquisa de doutorado na universidade Católica de Brasília (tecnologia como potencializadora da auto estima do aluno do ensino médio da escola pública no Brasil),
vejo todos os dias aportar nas mais diversas redes sociais pseudoespecialistas com mil e uma dicas e teorias acerca de como a tecnologia seria a salvação da educação, mas de concreto, tais especialistas nada tem a oferecer a não ser um pequeno momento de
autopromoção.
O Impacto das TIC, na educação é, na verdade, um aspecto particular de um fenômeno muito mais amplo, relacionado com o papel dessas tecnologias na atual sociedade da informação – SI , segundo Cool e Monero (2010), fazendo surgir novas modalidades de educação, formais ou informais, individuais ou coletivas, de natureza autodidata ou sob a tutela de instituições de ensino; em formato presencial, híbrido, ou totalmente mediado por tecnologias digitais, desenhando um novo cenário para a
educação. Como destacam Pallo9 & Pratt (2002), os objetivos, papéis, metodologias e recursos digitais estão sendo repensados à medida que máquinas, redes eletrônicas e tecnologias móveis invadem os espaços de aprendizagem tradicionais, fazendo emergir conceitos e práticas relacionadas a sistemas informatizados, ambientes hipermídia e comunidades virtuais de aprendizagem.
De acordo com César Coll (um dos principais coordenadores da reforma educacional espanhola), e Carles Monereo (Doutor em Psicologia na educação da Universidade Autônoma de Barcelona):
‘Estamos assistindo há décadas ao surgimento de uma nova forma de organização política, econômica, social e cultural identificada como sociedade da informação (SI), que comporta novas maneiras de trabalhar, comunicarse, de relacionar-se, de aprender, de pensar, e, em suma, de viver’. ‘E as TIC em sua dupla condição de causa e efeito, têm sido determinantes
nessa transformação’. Entre todas as tecnologias criadas pelos seres humanos, aquelas relacionadas com a capacidade de representar e transmitir a informação, ou seja, as
tecnologias da informação e da comunicação revestem-se de especial importância, porque afetam o dia a dia de alunos e professores. Vivemos em uma época em que as tic vão além da base comum do conteúdo. (CÉSAR e COLL, 2011, p17)
Nessa perspectiva, podemos considerar que a utilização significativa e crítica
de computadores e recursos digitais contribuem para a construção e apropriação de
conhecimentos dos sujeitos, ao permitir que professores e alunos possam compreender melhor sua realidade para transformá-la (JONASSEN, 2007).
Mas quando surgiram as tecnologias na educação? Após algumas pesquisas
como as realizadas na Libary University of Kansas pelos jornalistas Benjamin Innes
e Charles Wilson em seu artigo “The Learning Machines” para o New York times edição de 19 setembro de 2010, aponta alguns dados interessantes, pois quando falamos
em Tecnologias da Informação e Comunicação - TIC na educação, logo pensamos em
Tablets, Smartphones, netbooks etc. No entanto, a educação vive as voltas com as tecnologias desde 1650. Com aparatos como o Horn-Book (tratava-se de uma madeira com
impressos), utilizado para alfabetização de crianças e textos religiosos (era uma forma
na época colonial de ajudar as crianças a aprender a ler e escrever). Entre 1850 a 1870
tivemos outro aparato curioso: o Ferule (tratava-se de uma espécie de espeto de madeira
mais grosso, que servia como apontador/indicador). Tanto o Ferule (figura 2), como o
Horn-Book (figura 1), tinham dupla aplicação, serviam tanto para aprendizagem como
para castigo físico imputado á alunos dispersos e/ou que não conseguiam aprender as
lições. O que reforçava a ideia de uma educação punitiva (não muito distante dos dias
atuais, se compararmos os modelos de avaliação atuais com as punições físicas citadas).

![](../imagens/cp1.png)

Segundo Tardif (2015), tais tecnologias surgiram no plano da cultura material,
ou seja, com a descoberta dos processos de impressão de imagens (gravura sobre madeira). Fato que acarretou uma expansão progressiva da cultura escrita até as camadas
iletradas da sociedade, fazendo com que a cultura escolástica e dos clérigos deixassem
de ser o centro cultural social.
Ainda segundo o autor, surgia uma nova ordem econômica (o capitalismo e a
industrialização), assim, o surgimento do capitalismo industrial que vai do século XVIII
até XIX aproximadamente, provocaria também um descentramento profundo de antigas
práticas sociais, levando a educação a um novo patamar, que classi>co como fabril.
As tecnologias foram na verdade, muito variadas e de certa forma deram origem aos devices atuais. Continuando nossa jornada passamos pela MAGIC LANTERN (figura 3) em 1870, percussora do nosso projetor de slides. Passamos depois
pelo SCHOOL SLATE (figura 4) em 1890, seguido pelo CHALKBOARD (figura 5),ambos percussores do quadro negro/branco, também de 1890. Finalizamos a era das
criações com o LÁPIS (figura 6) em 1900. Deste ponto em diante, considero apenas
melhoramentos (aperfeiçoamento com base nas tecnologias já existentes), das invenções já descritas.

![](../imagens/cp2.png)

Continuando, tivemos em 1905 o ESTEREOSCOPE (modelo individual
do projetor de slides – figura 7), o FILM PROJECTOR (figura 8) em 1925 como
sendo o primeiro projetor de filmes (uma ideia melhorada do projetor de slides). Ainda em 1925 surge o RÁDIO (figura 9), seguido em 1930, pelo RETRO
PROJETOR (inicialmente utilizado na área militar – figura 10). Desde o rádio
considero (uma ideia pessoal), que a escola já estava na era da modernidade
tecnológica, assim, em 1940 surge a CANETA ESFEROGRÁFICA e o MIMEOGRAFO (figura 11).

![](../imagens/cp3.png)

Os VIDEOTAPES entram em cena 1951, acompanhados em 1957, de um instrumento pouco conhecido o ACELERADOR DE LEITURA (talvez o percussor da
MÁQUINA DE APRENDIZAGEM de SKINER também de 1957). Em 1958 surge a
TELEVISÃO EDUCATIVA, seguida pela FOTOCOPIADORA em 1959, que abre espaço para o nascimento do LIQUID PAPER em 1960.
De 1960 até os dias atuais uma enxurrada de tecnologia continua invadindo
nossas escolas, dentre elas destaco:       
1965 – Micro>lm                            
1970 – Calculadora Manual                   
1972 – Cartão perfurado                    
1980 – Computador pessoal ou computador de mesa           
1985 – CD ROM            
1999 – Quadro interativo            
2006 – O Computador por aluno – UCA                   
2010 – Apple IPAD

Como pode ser observado e ainda citando Cool e Moreno (2010), estamos
assistindo já há algumas décadas, o surgimento de uma nova forma de organização
econômica, social, política, cultural e educacional que atualmente chamamos de sociedade da informação (SI), que comporta novas maneiras de trabalhar, de comunicar-se,
de relacionar-se, de aprender, de pensar, em suma de VIVER e CONVIVER.
Somente na atualidade demos crédito das transformações às tic, sendo que na
verdade, as tic, há pelo menos três séculos têm assumido uma dupla condição de causa
e efeito em nossas escolas, e se tornaram fatores determinantes para a transformação
da atual sociedade.
Com seu papel notado e ampliado, as tics tem se tornado objeto de desejo e
compulsão para alguns educadores. E pior, tais educadores atribuem a elas a saída
para termos uma educação de qualidade e que atenda aos critérios da atual sociedade.
Tanto que Moraes (2011) coloca que, uma das a>rmações mais comuns hoje
em dia, é que o mundo está vivendo um processo de grandes transformações, profundas e aceleradas e que se modificam a cada instante.
É fato e ainda citando Moraes (2011), que não podemos negar que há uma
nova demanda para um novo tipo de tecnologia, que, por sua vez, vem sendo gerada
em decorrência do processo de maturação tecnológica e do desenvolvimento das telecomunicações.
Da mesma forma, não posso concordar 100% com Pierre Lévy (1999), quando
coloca que com a informatização surgiu um novo tipo de gestão social do conhecimento, na medida em que usamos um modelo digital que não é lido ou interpretado
como um texto clássico, mas, explorado de forma criativa.
Pois, para que um aluno tenha condições de ler, interpretar ou mesmo explorar
um texto, somente a tecnologia não basta. É necessário ter passado por um processo
formativo, com apoio de pro>ssionais dos mais diversos per>s, e que sejam realmente
especialistas com visão diferenciada, uma visão transdiciplinar. A>nal se pensarmos apenas em tecnologia, há 360 anos convivemos com ela e nossas escolas não mudaram
muito neste mesmo período.
Tanto uma escola, como um professor que queiram com as TIC provocar mudanças, necessitam de um novo per>l. Este novo per>l segundo Moraes (2010), implica mudanças na visão intelectual e social no papel do professor, pois ao trabalhar
com as TIC reconhece-se as incertezas e necessidades de fundamentar-se não só nas
disciplinas em que atuam, mas principalmente nos aspectos históricos locais, aceitando a inexistência de verdades absolutas e a presença de diferentes possíveis métodos
e metodologias de trabalho com as tic, o que naturalmente transformará a forma de
pensar, e sua compreensão social do mundo e da vida.
Não basta a tecnologia, é necessário uma formação adequada dos atores educacionais para que proporcionem as mudanças esperadas pela sociedade. Da mesma forma que, não basta á tecnologia presente em nossas escolas, é necessário proporcionar
um norte, uma “tutoria” para que esta nova geração possa usar todo seu conhecimento
tecnológico de forma a ampliar sua capacidade de ler, interpretar ou mesmo explorar
os conteúdos educacionais. Somente assim, se cria um vinculo direto a necessidade
atual do aluno, ou mesmo, a busca de soluções para problemas reais que emergem com
o novo conhecimento adquirido.
Uma tecnologia educacional como o computador ou a internet, por meio do recurso de redes interativas, favorece novas formas de acesso à informação e à comunicação, e
amplia as fontes de pesquisa em sala de aula, criando novas concepções dentro da realidade
atual, abrindo espaço para a entrada de novos mecanismos e ferramentas que facilitem as
ligações necessárias a fim de atender ao novo processo cognitivo do século XXI.
No entanto, é importante termos em mente a necessidade de uma formação
adequada para nossos professores, afinal segundo Cysneiros (2000, p.2) “nem todos os
aprendizes, sejam professores ou alunos, tem condições de descobrir espontaneamente
usos interessantes de so!ware”. As pessoas aprendem diferentemente umas das outras,
alguns podem aprender pela curiosidade, pelo manuseio, individualmente, outros necessitam de um suporte para obterem melhores resultados em sua aprendizagem.
Este novo cenário que para muitos é desconhecido, para nossos alunos é seu
habitat natural. Assim, podemos também nos fazer valer da tecnologia para criação de
uma nova forma de atuação, onde alunos e professores não tem lugar definido, ambos
são agentes ativos dos processos de ensino e aprendizagem.
Segundo Nóvoa (1997), a troca de experiências entre os alunos e docentes solidifica os espaços de formação conjunta, onde cada individuo é chamado a desempenhar, simultaneamente, o papel de formador e de formando.
Este novo olhar pode representar um novo momento epistemológico do sujeito.
Onde, segundo Tardif (2014), seremos reconhecidos como sujeitos do conhecimento e
verdadeiros atores sociais quando começarmos a reconhecer-nos uns aos outros como
pessoas competentes, pares iguais que podem aprender uns com os outros, respeitando o conhecimento do professor em sua área de atuação de forma simultânea em que
se reconhece o conhecimento do aluno com as novas tecnologias.
O saber fazer resulta da construção ou da articulação de um conhecimento que
opera em rede, produto de uma espiral recursiva que articula diferentes saberes. Afinal
como bem coloca Moraes (2010), vivemos um momento de mutação na relação sujeito objeto. Assim, quando as amarras do tradicionalismo pedagógico forem vencidas,
abriremos espaço para a criação e construção de um novo modelo de educação.
Mas devemos estar atentos a fala de Bauman (2013) quando acertadamente
coloca que nossa cultura baseada num insaciável apetite por novidade é a responsável
por não termos uma verdadeira revolução cultural, uma vez que os poderes do atual
sistema educacional estejam limitados, justamente pelo próprio sistema estar submetido ao jogo consumista, imputado à educação com o surgimento das TIC.
Neste ponto é importante colocar que em hipótese alguma sou contra a
utilização das TIC em sala de aula, mas sim, de uma utilização sem possibilidade
de replicabilidade e sem base cientifica de sua eficiência e eficácia no desenvolvimento da educação. O que não podemos aceitar é a máxima do mínimo aceitável,
ou seja, aceitar que por nossas escolas passarem tantos anos de privação, passem
a aceitar “qualquer” solução como sendo uma saída, afinal, sabe-se que anos de
privação nos fazem crer que a servidão passa a ser percebida como uma liberdade
de escolha.
Esta nova geração da internet, já entendeu que o segredo está na mobilidade e não no conteúdo (como ainda pregam alguns dos oportunistas da rede), pois o
conteúdo pouco mudou. Neste período em que se desenvolveu a internet e demais
tecnologias a única disciplina que teve seu conteúdo alterado foi à química (com a inclusão de quatro novos elementos) e assim mesmo, tal fato ocorreu já em 2016. Nossos
conteúdos não mudaram, nem a necessidade real de se aprendê-los.
Pois quando oportunizamos aos nossos alunos um novo formato de conteúdo (sem este necessariamente estar atrelado à forma tradicional), abrimos espaço
ao novo, ao diferente. Retomamos o processo de criação e motivação deste aluno.
Processo este, já muito visto e debatido desde os escritos de Comenius, que foram
replicados e aperfeiçoado por tantos outros grandes mestres como Juan Pozo, Frank
Laubach, Alvaro Vieira Pinto, Paulo Freire, entre outros. Os processos de ensino e aprendizagem vêm e vão, em um espiral recursivo onde de acordo com Maturana
(2001) faz com que a partir deste nosso viver cotidiano, escutemos alguém. No entanto, o que ouvimos é um acontecer interno a nós, e não o que o outro diz ou reproduz, embora o que ouvimos seja desencadeado pelo outro, a produção subjetiva
de significado a fala é nossa.
Assim sendo, o que nos falta é criar condições de formações de base e continuada, para que nossos professores consigam trabalhar com as TIC mediando os
processos de ensino e aprendizagem, buscando separar conteúdo de forma para que
possamos atingir e desperta a maioria de nossos alunos, por meio de um processo individual dentro do coletivo, unidade na diversidade de uma sala de aula. Algo possível
apenas por meio da tecnologia e com professores bem formados.

# Uso da tecnologia móvel na educação
![](../imagens/celular3.jpg)

Outro artigo que relata sobre o uso da tecnologia no meio educacional é o escrito por Leonardo Romão Pereira, Vera Rejane Niedersberg Schuhmacher, Elcio Schuhmacher e Oscar Dalfovo, este artigo tem como título: O USO DA TECNOLOGIA NA EDUCAÇÃO, PRIORIZANDO A TECNOLOGIA MÓVEL. E portanto trata principalmente da tecnologia móvel e de como ela pode ser introduzida na educação. Aqui segue o resumo deste artigo:  
O acentuado crescimento tecnológico e o alto consumo de smartphones (telefones com acesso a redes sociais e Internet) popularizam seu uso, em um público cada vez mais jovem. Para uma geração que nasceu submersa na tecnologia móvel, fazer o uso dos recursos disponíveis pode ser produtivo e eficiente no ambiente escolar. Embora o uso inadequado possa prejudicar o rendimento dos alunos, esses equipamentos, quando utilizados com objetivos educacionais específicos e definidos, são capazes de promover a interação e auxiliar no processo de ensino-aprendizagem, é o que aponta Machado (2010) que, ainda, afirma que esses dispositivos podem ser incluídos em projetos educacionais. Muito se fala que a tecnologia afasta as pessoas, pelo contrário, os meios acabam as envolvendo quando despertam o interesse e proporcionam o conhecimento. Moran (2007, p.9) justifica que “conectados multiplicam intensamente o número de possibilidades de pesquisa, de comunicação on-line, aprendizagem, compras, pagamentos e outros serviços”. É função da escola, educar e agregar valor ao uso desses aparelhos. Além do uso de computadores, se discute a possibilidade de uso da tecnologia móvel como ferramenta para auxiliar no processo de ensino. Apresentam-se diversas possibilidades de aliar esta tecnologia móvel à educação. Um dos pontos é à necessidade do professor possuir conhecimento e domínio sobre esta ferramenta, além de criatividade para desenvolver atividades e entretenimentos para os alunos. A ideia que se discuti é a incorporação desta tecnologia digital, principalmente a móvel, para promover a mobilidade na educação, pelo uso de aplicativos específicos e recursos disponíveis. Como já se ultrapassou a era da informação e hoje se vive a era do conhecimento, o objetivo é tirar o melhor proveito dos milhares de celulares disponíveis, usados pela maioria como meio de comunicação, principalmente dos alunos e inseri-los no contexto de ensino/aprendizagem como uma ferramenta, de forma a compartilhar experiências, transformar o conhecimento em valor e estimular o interesse no conteúdo abordado, fazendo com que o processo de ensino-aprendizagem seja algo agradável para o aluno, bem como para o educador. O objetivo do projeto foi o desenvolvimento de um Objeto de Aprendizagem (OA) para dispositivos móveis, como ferramenta de apoio ao processo de Ensino de Matemática no conteúdo de Aritmética, para os anos iniciais do Ensino Fundamental. Este trabalho trata da construção do objeto de aprendizagem denominado de “PACMATE” da sua concepção e disponibilização, com o objetivo de incentivar professores a fazer uso das Tecnologias de Informação e Comunicação (TIC) em ambiente escolar. A construção do OA seguiu os seguintes passos: Definição do objeto digital, compatibilidade com os ambientes dos smartphones, pois neste ambiente existe uma limitação de espaço em disco e na visualização. Utilização do OA como parte ou uma atividade específica de Matemática. Ter tempo limitado para uso do aluno, pois o objetivo do OA é a aprendizagem e não o resultado do jogo. Que o mesmo fosse planejado para ser utilizado em uma determinada aula, na qual se estaria ensinando o conteúdo de Aritmética. Na etapa de criação e desenvolvimento do mapa conceitual, que serviu para nortear e fundamentar o desenvolvimento do objeto de aprendizagem, foram apresentados os requisitos mínimos para que o OA alcance seu principal objetivo de apoio ao processo de ensino de Matemática. Após definidos os requisito, as técnicas, ferramentas e tecnologias a serem utilizadas, partiu-se para a realização e desenvolvimento do OA. Na etapa final foram realizados testes e análise de desempenho do aplicativo. O OA foi desenvolvido para a plataforma Android. A linguagem de programação Java foi utilizada para o desenvolvimento do jogo. O jogo passou ainda por um processo de validação junto ao público alvo proposto sendo considerados adequados para os seus propósitos. O AO se encontra disponível no Observatório da Educação Básica (OBEB), projeto financiado pelo INEP/CNPq, que disponibiliza em seu ambiente outros Objeto de Aprendizagem tais como mapas, cadernos temáticos e jogos a serem utilizados por professores do Ensino Básico. Além disso, o OBEB tem promove a formação didático/pedagógica de professores no uso das TIC por meio de cursos de formação[4]. 

## Finalização
Por fim, concluo este capítulo com a finalidade de ter exemplificado como a tecnologia pode ser utilizada na educação e ter aberto a oportunidade de leitura de alguns artigos interessantes sobre este tema.


# Referências: 
1. CHAVES, Eduardo OC. Tecnologia na educação. Encyclopaedia of Philosophy of Education, edited by Paulo Ghirardelli, Jr, and Michal A. Peteres. Published eletronically at, p. 14, 1999.  
2. MELO, Douglas et al. Robô euroi: Game de estratégia matemática para exercitar o pensamento computacional. In: Brazilian Symposium on Computers in Education (Simpósio Brasileiro de Informática na Educação-SBIE). 2018. p. 685.   
3. BRUZZI, Demerval Guilarducci. Uso da tecnologia na educação, da história à realidade atual. Revista Polyphonía, v. 27, n. 1, p. 475-483, 2016.  
4. PEREIRA, Leonardo Romão et al. O uso da tecnologia na educação, priorizando a tecnologia móvel. Acesso em, v. 16, 2012.

