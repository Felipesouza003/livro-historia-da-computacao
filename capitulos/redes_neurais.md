# Redes Neurais
## O que são redes neurais?
Redes neurais, também conhecidas como redes neurais artificiais ou simuladas, são um subgrupo de aprendizado de máquina e o coração dos algoritmos de aprendizado profundo. Seu nome e estrutura são baseados no cérebro humano, se fundamentando na maneira que os neurônios interagem entre si.

Essas redes são compostas de camadas nodulares, contendo uma camada de entrada, uma ou mais camadas ocultas, e uma camada de saída. Cada nó, ou neurônio artificial, se conecta a outro e contém o próprio peso e limite. Se a saída de qualquer nó individual é maior que o limite especificado, aquele nó é ativado, enviando dados ao próximo nó. Caso contrário, nenhum dado é enviado ao próximo nó.
![](../imagens/imagem1.png)
As redes neurais se apoiam em dados de treinamento para aprender e aumentar sua precisão com o tempo. No entanto, quando esses algoritmos de aprendizado estão aperfeiçoados, se tornam ferramentas poderosas na ciência computacional e inteligência artificial, nos permitindo classificar e agrupar dados rapidamente. Tarefas envolvendo reconhecimento de fala ou de imagens duram minutos ao invés de horas quando comparadas com identificação manual por profissionais. Uma das redes neurais mais conhecidas é o algoritmo de pesquisa do Google.

## Como funcionam as redes neurais?
Considere cada nó como seu próprio modelo de regressão linear, composto de dados de entrada, pesos, um limite e uma saída. A fórmula se pareceria com isso:
![](../imagens/IMAGEM2.png)
![](../imagens/imagem3.png)
w: Peso da variável

x:  Variável

bias: -Limite

output: Saída

Quando uma camada de entrada é determinada, pesos são definidos. Esses pesos determinam a importância de uma dada variável. Todas as variáveis são somadas e multiplicadas por seus respectivos pesos. Após isso, o resultado passa por uma função de ativação, que determina a saída. Se essa soma excede um certo limite, o nó é ativado, enviando dados á próxima camada. Sendo os dados de saída de um nó se tornando os dados de entrada do próximo nó.

Agora iremos exemplificar como um nó se pareceria usando valores binários. Nós podemos usar esse conceito com um exemplo mais tangível, por exemplo se você deveria ir surfar (Sim=1 e Não=0). A decisão é o nosso dado de saída. Vamos definir 3 fatores que influenciam a sua decisão:

1: As ondas estão boas? (Sim=1 e Não=0);

2: A fila está vazia? (Sim=1 e Não=0);

3: Houve algum ataque de tubarão recentemente? (Sim=0 e Não=1);
 
Agora, vamos definir as variáveis a partir dos seguintes dados de entrada:

X1=1, Pois as ondas estão absurdas hoje;

X2=0, Pois a fila está enorme;

X3=1, Pois não houve nenhum ataque de tubarão recentemente;

Agora, temos que definir o peso para determinar a importância de cada variável.

W1=5, Pois ondas boas não são comuns nessa praia;

W2=2, Pois você está acostumado com filas;

W3=4, Pois você tem medo de tubarões;

Para terminar, vamos definir um valor limite de 3, que traduziria em um bias de -3. Com todas as variáveis definidas, podemos usar a fórmula para chegar ao valor de saída.

Y-hat = (1* 5) + (0* 2) + (1* 4) -3 = 6

Se usarmos a função de ativação da segunda imagem, podemos definir que a saída deste nó seria 1, já que 6 é maior que 0. Dessa maneira, você iria surfar. No entanto, se ajustarmos os pesos ou o limite, poderíamos encontrar diferentes soluções para a decisão. Desta forma, podemos ver como a rede neural pode tomar decisões cada vez mais complexas.

## História das redes neurais

A história das redes neurais é maior do que a maioria das pessoas pensa. Mesmo que a ideia de uma “máquina que pensa” vem desde a Grécia antiga, focaremos nos eventos-chave que levaram a evolução das redes neurais.

1953: Warren S. McCulloch e Walter Pitts publicam “Cálculo Lógico das Ideias Inerentes na Atividade Nervosa”. Essa pesquisa buscou entender como o cérebro humano poderia produzir complexos padrões por neurônios interligados. Uma das principais ideias provenientes da pesquisa foi a comparação dos neurônios com o código binário.

1958: Frank Rosenblatt tem seu nome associado á criação do perceptron (a primeira rede neural criada, composta apenas por um “neurônio”.). Ele leva o trabalho de McCulloch e Pitts um passo á frente, introduzindo os pesos na equação.

1974: Paul Werbos se torna a primeira pessoa nos Estados Unidos a basear sua tese de doutorado em redes neurais.

1989: Yann LeCun publica um artigo ilustrando como o uso de retrições na retropropagação de erros em sua arquitetura de redes neurais poderia ser usada para treinar algoritmos. Essa pesquisa possibilitou a rede neural a distinguir códigos postais escritos a mão.

## Referências

IBM Cloud Education, Neural Networks (2020) https://www.ibm.com/cloud/learn/neural-networks#toc-history-of-rIfu5uF2